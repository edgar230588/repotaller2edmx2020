function getProductos(){
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function(){
    if (this.readyState == 4 && this.status == 200){
      console.table(JSON.parse(request.responseText).value);
      productosObtenidos = request.responseText;
      procesarProductos();
    }
  }
  request.open("GET", url, true);  //GET de consulta, true para respuesta syncrona
  request.send();
}
function procesarProductos(){
  var JSONProductos = JSON.parse(productosObtenidos);
//  alert(JSONProductos.value[0].ProductName); //chai es el nombre del producto en el arreglo Value

var divTabla = document.getElementById("divTablaProductos");
var table = document.createElment("table"); // para crear una tabla
var tbody = document.createElment("tbody");

tabla.classList.add("table"); //clase de css que es table
table.classList.add("table.striped"); //para crear table a rayas

for (var i = 0; i < JSONProductos.value.length; i++) { // valuea es el elemento que trae el arreglo
  var nuevaFila = document.createElement("tr");
  var columnaNombre = document.createElement("td");
  columnaNombre.innerText = JSONProductos.value[i].ProductName;

  var columnaPrecio = document.createElement("td");
  columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;

  var columnaStock = document.createElement("td");
  coumnaStock.innerText = JSONProductos.value[i].UnitsInStock;

  nuevaFila.appendChild(columnaNombre);
  nuevaFila.appendChild(columnaPrecio);
  nuevaFila.appendChild(columnaStock);
  tbody.appendChild(nuevaFila);

  }
  tabla.appendChild(tbody);
  divTabla.appendChild(table);

}
